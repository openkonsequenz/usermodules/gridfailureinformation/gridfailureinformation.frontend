/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
var fs = require('fs-extra');
var jsonConcat = require('json-concat');

var localizationSourceFilesDE = [
  './i18n/general.de.json',
  './i18n/components.de.json',
  './i18n/logout.de.json',
  './i18n/grid-failure.de.json',
  './i18n/distribution-group.de.json',
];

function mergeAndSaveJsonFiles(src, dest) {
  jsonConcat({ src: src, dest: dest }, function (res) {
    console.log('Localization files successfully merged!');
  });
}

function setEnvironment(configPath, environment) {
  fs.writeJson(configPath, { env: environment }, function (res) {
    console.log('Environment variable set to ' + environment);
  });
}

// Set environment variable to "development"
setEnvironment('./config/env.json', 'development');

// Merge all localization files into one
mergeAndSaveJsonFiles(localizationSourceFilesDE, './i18n/de.json');
