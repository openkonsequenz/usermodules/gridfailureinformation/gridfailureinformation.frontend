/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { IconCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';
import { ModeEnum } from '@grid-failure-information-app/shared/constants/enums';
import { of } from 'rxjs';
import { StateEnum } from '@grid-failure-information-app/shared/constants/enums';

describe('IconCellRendererComponent', () => {
  let component: IconCellRendererComponent;

  beforeEach(() => {
    component = new IconCellRendererComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return false when calling refesh', () => {
    expect(component.refresh()).toBeFalsy();
  });

  it('should init the renderer and show all the icons if there is an context icon and set editable flag', () => {
    const params: any = {
      context: {
        icons: { add: 'add', edit: 'edit', readonly: 'readonly', delete: 'delete', remove: 'remove', loadCondensedItems: 'loadCondensedItems' },
        eventSubject: {
          subscribe() {},
        },
      },
      data: { editable: true },
    };
    component.agInit(params);
    expect(component.editFlag).toBe(true);
    expect(component.addIcon).toBe(true);
    expect(component.readonlyIcon).toBe(true);
    expect(component.deleteIcon).toBe(true);
    expect(component.removeIcon).toBe(true);
    expect(component.condensedIcon).toBe(true);
    expect(component.editIcon).toBe(true);
  });

  it('should init the renderer and hide all the icons if there is not an context icon and set editable flag', () => {
    const params: any = {
      context: {
        icons: undefined,
        eventSubject: {
          subscribe() {},
        },
      },
      data: { editable: false },
    };
    component.agInit(params);
    expect(component.editFlag).toBe(false);
    expect(component.addIcon).toBe(null);
    expect(component.readonlyIcon).toBe(null);
    expect(component.deleteIcon).toBe(null);
    expect(component.removeIcon).toBe(null);
    expect(component.condensedIcon).toBe(null);
    expect(component.editIcon).toBe(null);
  });

  it('should call updateIcon and show/hide the right icons if mode is InitialMode and there are no context icons', () => {
    const condensed: boolean = true;
    const event: any = { eventType: ModeEnum.InitialMode };
    const params: any = {
      context: {
        eventSubject: of(event),
      },
      data: { condensed: condensed },
    };
    component.agInit(params);

    expect(component.addIcon).toBe(false);
    expect(component.removeIcon).toBe(false);
    expect(component.editIcon).toBe(true);
    expect(component.condensedIcon).toBe(condensed);
  });

  it('should call updateIcon and show/hide the right icons if mode is OverviewTableSelectionMode and there are no context icons', () => {
    const condensed: boolean = true;
    const event: any = { eventType: ModeEnum.OverviewTableSelectionMode };
    const params: any = {
      context: {
        eventSubject: of(event),
      },
      data: { condensed: condensed },
    };
    component.agInit(params);

    expect(component.addIcon).toBe(!condensed);
    expect(component.editIcon).toBe(false);
    expect(component.condensedIcon).toBe(false);
  });

  it('should call updateIcon and show/hide the right icons if mode is OverviewTableSelectionMode and there are no context icons', () => {
    const condensed: boolean = false;
    const publicationStatusPublished = StateEnum.PUBLISHED;
    const event: any = { eventType: ModeEnum.OverviewTableSelectionMode };
    const params: any = {
      context: {
        eventSubject: of(event),
      },
      data: { condensed: condensed, publicationStatus: publicationStatusPublished },
    };
    component.agInit(params);

    expect(component.addIcon).toBe(false);
    expect(component.editIcon).toBe(false);
    expect(component.condensedIcon).toBe(false);
  });

  it('should call updateIcon and show/hide the right icons if mode is CondensationTableSelectionMode and there are no context icons', () => {
    const condensed: boolean = true;
    const event: any = { eventType: ModeEnum.CondensationTableSelectionMode };
    const params: any = {
      context: {
        eventSubject: of(event),
      },
      data: { condensed: condensed },
    };
    component.agInit(params);

    expect(component.editIcon).toBe(false);
    expect(component.removeIcon).toBe(true);
  });

  it('should call updateIcon and dont set addIcon if mode is anything else and there are no context icons', () => {
    const dontChangedAddIconValue: boolean = true;
    component.addIcon = dontChangedAddIconValue;
    const event: any = { eventType: 'im_not_initial_and_not_overview_mode' };
    const params: any = {
      context: {
        eventSubject: of(event),
      },
      data: {},
    };
    component.agInit(params);

    expect(component.addIcon).toBe(dontChangedAddIconValue);
  });

  it('should call updateIcon() and hide the delete icons if mode is oldVersionMode', () => {
    const event: any = { eventType: ModeEnum.oldVersionMode };
    const params: any = {
      context: {
        eventSubject: of(event),
      },
      data: {},
    };
    component.agInit(params);

    expect(component.deleteIcon).toBe(false);
  });

  it('should call updateIcon() and show the delete icons if mode is currentVersionMode', () => {
    const event: any = { eventType: ModeEnum.currentVersionMode };
    const params: any = {
      context: {
        eventSubject: of(event),
      },
      data: {},
    };
    component.agInit(params);

    expect(component.deleteIcon).toBe(true);
  });

  it('should return false if call refresh function', () => {
    const returnValue = component.refresh();
    expect(returnValue).toBeFalsy();
  });

  it('should transfer the updated values onInit ', () => {
    component.params = {
      context: {
        eventSubject: { next: function () {} },
      },
    };
    const spy = spyOn(component.params.context.eventSubject, 'next');
    component.ngOnInit();

    expect(spy).toHaveBeenCalledWith({ type: 'initialLoad' });
  });

  it('should transfer the updated values on click ', () => {
    const eventType: string = 'im_an_event_type';
    component.params = {
      context: {
        eventSubject: { next: function () {} },
      },
      data: 'data',
    };
    const spy = spyOn(component.params.context.eventSubject, 'next');
    component.clicked(eventType);

    expect(spy).toHaveBeenCalledWith({ type: eventType, data: 'data' });
  });
});
