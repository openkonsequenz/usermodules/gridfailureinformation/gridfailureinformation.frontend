/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { GridFailureApiClient } from '@grid-failure-information-app/pages/grid-failure/grid-failure-api-client';
import {
  DistributionGroup,
  FailureAddress,
  FailureBranch,
  FailureClassification,
  FailureExpectedReason,
  FailureHousenumber,
  FailureRadius,
  FailureState,
  FailureStation,
  GridFailure,
  PublicationChannel,
} from '@grid-failure-information-app/shared/models';
import * as gridFailureActions from '@grid-failure-information-app/shared/store/actions/grid-failures.action';
import { sortItems } from '@grid-failure-information-app/shared/utility';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs';
import { catchError, concatMap, exhaustMap, map } from 'rxjs/operators';

@Injectable()
export class GridFailuresEffects {
  getGridFailures$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadGridFailures),
      exhaustMap(() => {
        return this._apiClient.getGridFailures().pipe(
          map(item => gridFailureActions.loadGridFailuresSuccess({ payload: item })),
          catchError(error => of(gridFailureActions.loadGridFailuresFail({ payload: error })))
        );
      })
    )
  );

  getGridFailureDetails$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadGridFailureDetail),
      exhaustMap(action => {
        return this._apiClient
          .getGridFailureDetails(action.payload)
          .pipe(
            map((gridFailureDetails: GridFailure) => gridFailureActions.loadGridFailureDetailSuccess({ payload: gridFailureDetails })),
            catchError(error => of(gridFailureActions.loadGridFailureDetailFail({ payload: error })))
          )
      })
    )
  );

  saveGridFailure$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.saveGridFailure),
      map(action => action['payload']),
      exhaustMap((payload: { gridFailure: GridFailure; saveForPublish: boolean }) => {
        return (
          payload.gridFailure.id
            ? this._apiClient.putGridFailure(payload.gridFailure.id, payload.gridFailure, payload.saveForPublish)
            : this._apiClient.postGridFailure(payload.gridFailure)
        ).pipe(
          map((item: GridFailure) => {
            return gridFailureActions.saveGridFailureSuccess({ payload: item });
          }),
          catchError(error => of(gridFailureActions.saveGridFailureFail({ payload: error })))
        );
      })
    )
  );

  deleteGridFailure$: any = createEffect(() => {
    return this._actions$.pipe(
      ofType(gridFailureActions.deleteGridFailure),
      exhaustMap(action => {
        const httpResult$ = this._apiClient.deleteGridFailure(action.gridFailureId);
        const error$ = error => of(gridFailureActions.deleteGridFailureFail({ error: error }));
        return httpResult$.pipe(
          map(() => gridFailureActions.deleteGridFailureSuccess()),
          catchError(error$)
        );
      })
    );
  });

  getGridFailureVersions$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadGridFailureVersions),
      exhaustMap(action => {
        return this._apiClient
          .getGridFailureVersions(action.payload)
          .pipe(
            map((gridFailureVersions: GridFailure[]) => gridFailureActions.loadGridFailureVersionsSuccess({ payload: gridFailureVersions })),
            catchError(error => of(gridFailureActions.loadGridFailureVersionsFail({ payload: error })))
          )
      })
    )
  );

  getGridFailureVersion$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadGridFailureVersion),
      exhaustMap(action => {
        return this._apiClient
          .getGridFailureVersion(action.gridFailureId, action.versionNumber)
          .pipe(
            map((gridFailureVersion: GridFailure) => gridFailureActions.loadGridFailureVersionSuccess({ payload: gridFailureVersion })),
            catchError(error => of(gridFailureActions.loadGridFailureVersionFail({ payload: error })))
          )
      })
    )
  );

  getGridFailureBranches$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadGridFailureBranches),
      exhaustMap(action => {
        return this._apiClient
          .getGridFailureBranches()
          .pipe(
            map((branches: FailureBranch[]) => gridFailureActions.loadGridFailureBranchesSuccess({ payload: branches })),
            catchError(error => of(gridFailureActions.loadGridFailureBranchesFail({ payload: error })))
          )
      })
    )
  );

  getGridFailureClassifications$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadGridFailureClassifications),
      exhaustMap(action => {
        return this._apiClient
          .getGridFailureClassifications()
          .pipe(
            map((classifications: FailureClassification[]) => gridFailureActions.loadGridFailureClassificationsSuccess({ payload: classifications })),
            catchError(error => of(gridFailureActions.loadGridFailureClassificationsFail({ payload: error })))
          )
      })
    )
  );

  getGridFailureStates$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadGridFailureStates),
      exhaustMap(action => {
        return this._apiClient
          .getGridFailureStates()
          .pipe(
            map((states: FailureState[]) => gridFailureActions.loadGridFailureStatesSuccess({ payload: states })),
            catchError(error => of(gridFailureActions.loadGridFailureStatesFail({ payload: error })))
          )
      })
    )
  );

  getGridFailureRadii$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadGridFailureRadii),
      exhaustMap(action => {
        return this._apiClient
          .getGridFailureRadii()
          .pipe(
            map((response: FailureRadius[]) => gridFailureActions.loadGridFailureRadiiSuccess({ payload: response })),
            catchError(error => of(gridFailureActions.loadGridFailureRadiiFail({ payload: error })))
          )
      })
    )
  );

  getGridFailureExpectedReasons$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadGridFailureExpectedReasons),
      exhaustMap(action => {
        return this._apiClient
          .getGridFailureExpectedReasons(action.payload)
          .pipe(
            map((response: FailureExpectedReason[]) => gridFailureActions.loadGridFailureExpectedReasonsSuccess({ payload: response })),
            catchError(error => of(gridFailureActions.loadGridFailureExpectedReasonsFail({ payload: error })))
          )
      })
    )
  );

  postGridFailuresCondensation$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.postGridFailuresCondensation),
      map(action => action['payload']),
      exhaustMap((payload: string[]) => {
        return this._apiClient.postGridFailuresCondensation(payload).pipe(
          map(() => {
            this._store.dispatch(gridFailureActions.loadGridFailures());
            return gridFailureActions.postGridFailuresCondensationSuccess();
          }),
          catchError(error => of(gridFailureActions.postGridFailuresCondensationFail({ payload: error })))
        );
      })
    )
  );

  getCondensedGridFailures$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadCondensedGridFailures),
      exhaustMap(action => {
        return this._apiClient.getCondensedGridFailures(action.payload).pipe(
          map(item => gridFailureActions.loadCondensedGridFailuresSuccess({ payload: item })),
          catchError(error => of(gridFailureActions.loadCondensedGridFailuresFail({ payload: error })))
        );
      })
    )
  );

  putGridFailuresCondensation$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.putGridFailuresCondensation),
      exhaustMap(action => {
        return this._apiClient.putGridFailuresCondensation(action.gridFailureId, action.payload).pipe(
          map((items: GridFailure[]) => {
            this._store.dispatch(gridFailureActions.loadGridFailures());
            return gridFailureActions.putGridFailuresCondensationSuccess({ payload: items });
          }),
          catchError(error => of(gridFailureActions.putGridFailuresCondensationFail({ payload: error })))
        );
      })
    )
  );

  getStations$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadStations),
      exhaustMap(action => {
        return this._apiClient
          .getStations()
          .pipe(
            map((response: FailureStation[]) => gridFailureActions.loadStationsSuccess({ payload: response })),
            catchError(error => of(gridFailureActions.loadStationsFail({ payload: error })))
          )
      })
    )
  );

  getGridFailurePolygon$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadGridFailurePolygon),
      exhaustMap(action => {
        return this._apiClient
          .getGridFailurePolygon(action.payload)
          .pipe(
            map((gridFailurePolygon: Array<{ stationId: string; polygonCoordinatesList: Array<[Number, Number]> }>) => {
              return gridFailureActions.loadGridFailurePolygonSuccess({ payload: gridFailurePolygon });
            }),
            catchError(error => of(gridFailureActions.loadGridFailurePolygonFail({ payload: error })))
          )
      })
    )
  );

  getGridFailureStations$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadGridFailureStations),
      exhaustMap(action => {
        return this._apiClient
          .getGridFailureStations(action.payload)
          .pipe(
            map((response: FailureStation[]) => gridFailureActions.loadGridFailureStationsSuccess({ payload: response })),
            catchError(error => of(gridFailureActions.loadGridFailureStationsFail({ payload: error })))
          )
      })
    )
  );

  getHistGridFailureStations$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadHistGridFailureStations),
      exhaustMap(action => {
        return this._apiClient
          .getHistGridFailureStations(action.failureId, action.versionNumber)
          .pipe(
            map((response: FailureStation[]) => gridFailureActions.loadHistGridFailureStationsSuccess({ payload: response })),
            catchError(error => of(gridFailureActions.loadHistGridFailureStationsFail({ payload: error })))
          )
      })
    )
  );

  postGridFailureStation$: any = createEffect(() => {
    let gridFailureId: string = '';
    return this._actions$.pipe(
      ofType(gridFailureActions.postGridFailureStation),
      exhaustMap(action => {
        gridFailureId = action.gridFailureDetailId;
        return this._apiClient.postGridFailureStation(action.gridFailureDetailId, action.station).pipe(
          map(() => {
            this._store.dispatch(gridFailureActions.loadGridFailureStations({ payload: gridFailureId }));
            return gridFailureActions.postGridFailureStationSuccess();
          }),
          catchError(error => of(gridFailureActions.postGridFailureStationFail({ payload: error })))
        );
      })
    );
  });

  deleteGridFailureStation$: any = createEffect(() => {
    let gridFailureId: string = '';
    return this._actions$.pipe(
      ofType(gridFailureActions.deleteGridFailureStation),
      exhaustMap(action => {
        gridFailureId = action.gridFailureDetailId;
        return this._apiClient.deleteGridFailureStation(action.gridFailureDetailId, action.stationId).pipe(
          map(() => {
            this._store.dispatch(gridFailureActions.loadGridFailureStations({ payload: gridFailureId }));
            return gridFailureActions.deleteGridFailureStationSuccess();
          }),
          catchError(error => of(gridFailureActions.deleteGridFailureStationFail({ payload: error })))
        );
      })
    );
  });

  getAddressPostalcodes$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadAddressPostalcodes),
      exhaustMap(action => {
        if (!action.community || !action.district) {
          return of(gridFailureActions.loadAddressPostalcodesSuccess({ payload: [] }));
        }
        return this._apiClient
          .getAddressPostalcodes(action.branch, action.community, action.district)
          .pipe(
            map((response: string[]) => sortItems(gridFailureActions.loadAddressPostalcodesSuccess({ payload: response }))),
            catchError(error => of(gridFailureActions.loadAddressPostalcodesFail({ payload: error })))
          )
      })
    )
  );

  getAllAddressCommunities$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadAllAddressCommunities),
      exhaustMap(action => {
        const allAdressCommunities: Observable<string[]> = this._apiClient.getAllAddressCommunities(action.branch);
        return allAdressCommunities
          .pipe(
            map((response: string[]) => sortItems(gridFailureActions.loadAllAddressCommunitiesSuccess({ payload: response }))),
            catchError(error => of(gridFailureActions.loadAllAddressCommunitiesFail({ payload: error })))
          )
      })
    )
  );

  getAddressDistricts$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadAddressDistrictsOfCommunity),
      exhaustMap(action => {
        if (!action.community) {
          return of(gridFailureActions.loadAddressDistrictsOfCommunitySuccess({ payload: [] }));
        }
        return this._apiClient
          .getAddressDistrictsOfCommunity(action.community, action.branch)
          .pipe(
            map((response: string[]) => sortItems(gridFailureActions.loadAddressDistrictsOfCommunitySuccess({ payload: response }))),
            catchError(error => of(gridFailureActions.loadAddressDistrictsOfCommunityFail({ payload: error })))
          )
      })
    )
  );

  getAddressStreets$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadAddressStreets),
      exhaustMap(action => {
        if (!action.postcode || !action.community || !action.district) {
          return of(gridFailureActions.loadAddressStreetsSuccess({ payload: [] }));
        }
        return this._apiClient
          .getAddressStreets(action.postcode, action.community, action.district, action.branch)
          .pipe(
            map((response: string[]) => sortItems(gridFailureActions.loadAddressStreetsSuccess({ payload: response }))),
            catchError(error => of(gridFailureActions.loadAddressStreetsFail({ payload: error })))
          )
      })
    )
  );

  getAddressHousenumbers$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadAddressHouseNumbers),
      exhaustMap(action => {
        if (!action.postcode || !action.community || !action.street) {
          return of(gridFailureActions.loadAddressHouseNumbersSuccess({ payload: [] }));
        }
        return this._apiClient
          .getAddressHousenumbers(action.postcode, action.community, action.street, action.branch)
          .pipe(
            map((response: FailureHousenumber[]) => gridFailureActions.loadAddressHouseNumbersSuccess({ payload: response })),
            catchError(error => of(gridFailureActions.loadAddressHouseNumbersFail({ payload: error })))
          )
      })
    )
  );

  getGridFailureAddress$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadGridFailureAddress),
      exhaustMap(action => {
        return this._apiClient
          .getAddress(action.payload)
          .pipe(
            map((gridFailureAddress: FailureAddress) => gridFailureActions.loadGridFailureAddressSuccess({ payload: gridFailureAddress })),
            catchError(error => of(gridFailureActions.loadGridFailureAddressFail({ payload: error })))
          )
      })
    )
  );

  getGridFailureDistributionGroups$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadGridFailureDistributionGroups),
      exhaustMap(action => {
        return this._apiClient
          .getGridFailureDistributionGroups(action.payload)
          .pipe(
            map((distributionGroups: DistributionGroup[]) => gridFailureActions.loadGridFailureDistributionGroupsSuccess({ payload: distributionGroups })),
            catchError(error => of(gridFailureActions.loadGridFailureDistributionGroupsFail({ payload: error })))
          )
      })
    )
  );

  createDistributionGroupAssignment$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.createDistributionGroupAssignment),
      concatMap(action => {
        return this._apiClient.postDistributionGroupAssignment(action.gridFailureId, action.newGroup).pipe(
          map(item => {
            this._store.dispatch(gridFailureActions.loadGridFailureDistributionGroups({ payload: action.gridFailureId }));
            return gridFailureActions.createDistributionGroupAssignmentSuccess({ payload: item });
          }),
          catchError(error => of(gridFailureActions.createDistributionGroupAssignmentFail({ payload: error })))
        );
      })
    )
  );

  deleteDistributionGroupAssignment$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.deleteDistributionGroupAssignment),
      exhaustMap(action => {
        return this._apiClient.deleteDistributionGroupAssignment(action.gridFailureId, action.groupId).pipe(
          map(item => {
            this._store.dispatch(gridFailureActions.loadGridFailureDistributionGroups({ payload: action.gridFailureId }));
            return gridFailureActions.deleteDistributionGroupAssignmentSuccess();
          }),
          catchError(error => of(gridFailureActions.deleteDistributionGroupAssignmentFail({ payload: error })))
        );
      })
    )
  );

  getGridFailurePublicationChannels$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadGridFailurePublicationChannels),
      exhaustMap(action => {
        return this._apiClient
          .getGridFailurePublicationChannels(action.payload)
          .pipe(
            map((publicationChannels: PublicationChannel[]) => gridFailureActions.loadGridFailurePublicationChannelsSuccess({ payload: publicationChannels })),
            catchError(error => of(gridFailureActions.loadGridFailurePublicationChannelsFail({ payload: error })))
          )
      })
    )
  );

  createPublicationChannelAssignment$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.createPublicationChannelAssignment),
      exhaustMap(action => {
        return this._apiClient.postPublicationChannelAssignment(action.gridFailureId, action.channel).pipe(
          map(item => {
            this._store.dispatch(gridFailureActions.loadGridFailurePublicationChannels({ payload: action.gridFailureId }));
            return gridFailureActions.createPublicationChannelAssignmentSuccess({ payload: item });
          }),
          catchError(error => of(gridFailureActions.createPublicationChannelAssignmentFail({ payload: error })))
        );
      })
    )
  );

  deletePublicationChannelAssignment$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.deletePublicationChannelAssignment),
      exhaustMap(action => {
        return this._apiClient.deletePublicationChannelAssignment(action.gridFailureId, action.channel).pipe(
          map(item => {
            this._store.dispatch(gridFailureActions.loadGridFailurePublicationChannels({ payload: action.gridFailureId }));
            return gridFailureActions.deletePublicationChannelAssignmentSuccess();
          }),
          catchError(error => of(gridFailureActions.deletePublicationChannelAssignmentFail({ payload: error })))
        );
      })
    )
  );

  getFailureReminder$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadFailureReminder),
      exhaustMap(action => {
        return this._apiClient
          .getFailureReminder()
          .pipe(
            map((isReminderActive: boolean) => gridFailureActions.loadFailureReminderSuccess({ payload: isReminderActive })),
            catchError(error => of(gridFailureActions.loadFailureReminderFail({ payload: error })))
          )
      })
    )
  );

  getStationCount$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(gridFailureActions.loadStationCount),
      exhaustMap(action => {
        return this._apiClient
          .getStationCount(action['stationIds'])
          .pipe(
            map((count: number) => gridFailureActions.loadStationCountSuccess({ payload: count })),
            catchError(error => of(gridFailureActions.loadStationCountFail({ payload: error })))
          )
      })
    )
  );

  constructor(private _actions$: Actions, private _apiClient: GridFailureApiClient, private _store: Store<any>) {}
}
