/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {
  GridFailurePolygonReducer,
  INITIAL_STATE,
  getData,
  getLoading,
  getLoaded,
  getFailed,
  reducer,
} from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-polygon.reducer';
import * as gridFailureActions from '@grid-failure-information-app/shared/store/actions/grid-failures.action';

describe('GridFailurePolygonReducer', () => {
  it('should return the initial state', () => {
    const action = { type: 'NOOP' } as any;
    const result = reducer(undefined, action);

    expect(result).toBe(INITIAL_STATE);
  });

  it('should trigger loading state', () => {
    const action = gridFailureActions.loadGridFailurePolygon({ payload: ['xx'] });
    const result = GridFailurePolygonReducer(INITIAL_STATE, action);

    expect(result).toEqual({
      ...INITIAL_STATE,
      loading: true,
    });
  });

  it('should trigger loaded state', () => {
    const action = gridFailureActions.loadGridFailurePolygonSuccess({
      payload: [
        {
          stationId: 'ba8f6d3a-f313-4fea-9f52-f8c1f43169cb',
          polygonCoordinatesList: [
            [49.720664, 8.336992],
            [49.737565, 8.351669],
            [49.741734, 8.372011],
            [49.722332, 8.379221],
            [49.720664, 8.336992],
          ],
        },
      ],
    });
    const result = GridFailurePolygonReducer(INITIAL_STATE, action);

    expect(result.loaded).toBe(true);
  });

  it('should trigger failed state', () => {
    const error = { payload: 'err_msg' };
    const action = gridFailureActions.loadGridFailurePolygonFail(error);
    const result = GridFailurePolygonReducer(INITIAL_STATE, action);

    expect(result).toEqual({
      ...INITIAL_STATE,
      failed: true,
    });
  });

  it('getData return state.data', () => {
    const state = { ...INITIAL_STATE };
    const result = getData(state);
    expect(result).toBe(state.data);
  });

  it('getLoading return state.loading', () => {
    const state = { ...INITIAL_STATE };
    const result = getLoading(state);
    expect(result).toBe(state.loading);
  });

  it('getLoaded return state.loaded', () => {
    const state = { ...INITIAL_STATE };
    const result = getLoaded(state);
    expect(result).toBe(state.loaded);
  });

  it('getFailed return state.failed', () => {
    const state = { ...INITIAL_STATE };
    const result = getFailed(state);
    expect(result).toBe(state.failed);
  });
});
