/********************************************************************************
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { AfterViewChecked, Component, Input, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, UntypedFormArray } from '@angular/forms';
import { DistributionGroupSandbox } from '../distribution-group.sandbox';

@Component({
  selector: 'app-distribution-group-allowed',
  templateUrl: './distribution-group-allowed.component.html',
  styleUrls: ['./distribution-group-allowed.component.scss'],
})
export class DistributionGroupAllowedComponent implements AfterViewChecked, OnInit {
  @Input()
  allowedGroups = null;

  internGroups = [];

  form: UntypedFormArray = new UntypedFormArray([]);

  constructor(public sandbox: DistributionGroupSandbox) {}

  ngOnInit(): void {
    this.setFormState();
  }

  setFormState() {
    const rows: UntypedFormGroup[] = [];

    for (let i = 0; i < this.internGroups.length; i++) {
      const row = {};
      row['id'] = new UntypedFormControl(i);
      row['classificationId'] = new UntypedFormControl(this.internGroups[i].classificationId);
      row['branchId'] = new UntypedFormControl(this.internGroups[i].branchId);
      row['autoSet'] = new UntypedFormControl(this.internGroups[i].autoSet);
      row['deleted'] = new UntypedFormControl(this.internGroups[i].deleted);

      rows.push(new UntypedFormGroup(row));
    }

    this.form = new UntypedFormArray(rows);
  }

  addRow() {
    this.internGroups.push({
      classificationId: '',
      branchId: '',
      deleted: false,
    });
    this.setFormState();
  }

  updateElem(rowId: number) {
    this.allowedGroups[rowId].branchId = this.form.controls[rowId].get('branchId').value;
    this.allowedGroups[rowId].classificationId = this.form.controls[rowId].get('classificationId').value;
    this.allowedGroups[rowId].autoSet = this.form.controls[rowId].get('autoSet').value;
  }

  delete(rowId: number) {
    if (this.allowedGroups[rowId].id) {
      this.allowedGroups[rowId].deleted = !this.allowedGroups[rowId].deleted;
    } else {
      this.allowedGroups.splice(rowId, 1);
    }
    this.setFormState();
  }

  ngAfterViewChecked() {
    if (this.allowedGroups && this.allowedGroups !== this.internGroups) {
      setTimeout(() => {
        this.internGroups = this.allowedGroups;
        this.setFormState();
      });
    }
  }
}
