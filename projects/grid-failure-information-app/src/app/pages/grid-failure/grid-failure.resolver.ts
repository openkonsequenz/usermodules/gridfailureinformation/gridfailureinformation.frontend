/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { GridFailureSandbox } from '@grid-failure-information-app/pages/grid-failure/grid-failure-list/grid-failure.sandbox';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { GridFailureDetailsSandbox } from '@grid-failure-information-app/pages/grid-failure/grid-failure-details/grid-failure-details.sandbox';
import { combineLatest, Observable, of } from 'rxjs';
import { map, skipWhile, take } from 'rxjs/operators';

@Injectable()
export class GridFailuresResolver  {
  constructor(private _sandbox: GridFailureSandbox, private _detailSandbox: GridFailureDetailsSandbox) {}

  public resolve(route: ActivatedRouteSnapshot): Observable<boolean> {
    const gridFailureId: string = route.params['gridFailureId'];
    if (!gridFailureId || (gridFailureId && gridFailureId === 'new')) {
      this._detailSandbox.setGridFailureId(null);
      this._detailSandbox.loadGridFailureExpectedReasons(null);
      this._sandbox.loadGridFailures();
      this._detailSandbox.settingsLoaded$ = of(true);
      this._detailSandbox.gridFailureDistributionGroupsLoaded$ = of(true);
      this._detailSandbox.gridFailurePublicationChannelsLoaded$ = of(true);
      this._detailSandbox.stationsLoaded$ = of(true);
      this._detailSandbox.gridFailureStationsLoaded$ = of(true);
    } else if (gridFailureId && gridFailureId !== 'new') {
      this._detailSandbox.loadPreConfiguration();

      this._detailSandbox.setGridFailureId(gridFailureId);
      // load current version
      this._detailSandbox.loadGridFailure(gridFailureId);
      // load history
      this._detailSandbox.loadGridFailureVersions(gridFailureId);
      // load distribution group
      this._detailSandbox.loadGridFailureDistributionGroups(gridFailureId);
      // load publication channels
      this._detailSandbox.loadGridFailurePublicationChannels(gridFailureId);
      // load stations to specific gridFailure
      this._detailSandbox.loadGridFailureStations(gridFailureId);
    }

    this._detailSandbox.loadGridFailureBranches();
    this._detailSandbox.loadGridFailureClassifications();
    this._detailSandbox.loadGridFailureStates();
    this._detailSandbox.loadGridFailureRadii();
    this._detailSandbox.loadGridFailureCommunities();
    this._detailSandbox.loadStations();
    this._detailSandbox.loadDistributionGroups();

    return combineLatest([
      this._detailSandbox.settingsLoaded$,
      this._detailSandbox.gridFailureDistributionGroupsLoaded$,
      this._detailSandbox.gridFailurePublicationChannelsLoaded$,
      this._detailSandbox.stationsLoaded$,
      this._detailSandbox.gridFailureStationsLoaded$,
    ]).pipe(
      skipWhile(([sl, dgl, pcl, stl, gfsl]) => !sl || !dgl || !pcl || !stl || !gfsl),
      map(([sl, dgl, pcl, stl, gfsl]) => sl && dgl && pcl && stl && gfsl),
      take(1)
    );
  }
}
