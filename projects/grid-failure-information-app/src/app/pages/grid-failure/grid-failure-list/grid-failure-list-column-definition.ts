/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DateTimeCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/date-time-cell-renderer/date-time-cell-renderer.component';
import { IconCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/icon-cell-renderer/icon-cell-renderer.component';
import { dateTimeComparator, sortAlphaNum } from '@grid-failure-information-app/shared/utility';
import { stringInsensitiveComparator } from '@grid-failure-information-table-app/app/utilityHelpers';
import { ColDef } from 'ag-grid-community';

export const GRID_FAILURE_COLDEF: ColDef[] = [
  {
    field: 'failureClassification',
    colId: 'failureClassification',
    headerName: 'GridFailure.FailureClassification',
    sortable: true,
    comparator: stringInsensitiveComparator,
    filter: 'setFilterComponent',
    suppressMovable: true,
  },
  {
    field: 'branch',
    colId: 'branch',
    headerName: 'GridFailure.Branch',
    sortable: true,
    comparator: stringInsensitiveComparator,
    filter: 'setFilterComponent',
    suppressMovable: true,
  },
  {
    field: 'expectedReasonText',
    colId: 'expectedReasonText',
    headerName: 'GridFailure.ExpectedReason',
    sortable: true,
    comparator: stringInsensitiveComparator,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'responsibility',
    colId: 'responsibility',
    headerName: 'GridFailure.Responsibility',
    sortable: true,
    comparator: stringInsensitiveComparator,
    filter: 'setFilterComponent',
    suppressMovable: true,
  },
  {
    field: 'statusIntern',
    colId: 'statusIntern',
    headerName: 'GridFailure.StatusIntern',
    sortable: true,
    comparator: stringInsensitiveComparator,
    filter: 'setFilterComponent',
    suppressMovable: true,
  },
  {
    field: 'publicationStatus',
    colId: 'publicationStatus',
    headerName: 'GridFailure.PublicationStatus',
    sortable: true,
    comparator: stringInsensitiveComparator,
    filter: 'setFilterComponent',
    suppressMovable: true,
  },
  {
    field: 'description',
    colId: 'description',
    headerName: 'GridFailure.Description',
    maxWidth: 300,
    sortable: true,
    comparator: stringInsensitiveComparator,
    filter: 'setFilterComponent',
    suppressMovable: true,
  },
  {
    field: 'voltageLevel',
    colId: 'voltageLevel',
    headerName: 'GridFailure.VoltageLevel',
    sortable: true,
    comparator: stringInsensitiveComparator,
    filter: 'setFilterComponent',
    suppressMovable: true,
  },
  {
    field: 'pressureLevel',
    colId: 'pressureLevel',
    headerName: 'GridFailure.PressureLevel',
    sortable: true,
    comparator: stringInsensitiveComparator,
    filter: 'setFilterComponent',
    suppressMovable: true,
  },
  {
    field: 'failureBegin',
    colId: 'failureBegin',
    headerName: 'GridFailure.FailureBegin',
    sortable: true,
    suppressMovable: true,
    filter: 'agDateColumnFilter',
    filterParams: {
      comparator: dateTimeComparator,
      browserDatePicker: true,
      buttons: ['apply', 'reset'],
      closeOnApply: false,
    },
    cellRenderer: DateTimeCellRendererComponent,
  },
  {
    field: 'failureEndPlanned',
    colId: 'failureEndPlanned',
    headerName: 'GridFailure.FailureEndPlanned',
    sortable: true,
    suppressMovable: true,
    filter: 'agDateColumnFilter',
    filterParams: {
      comparator: dateTimeComparator,
      browserDatePicker: true,
      buttons: ['apply', 'reset'],
      closeOnApply: false,
    },
    cellRenderer: DateTimeCellRendererComponent,
  },
  {
    field: 'internalRemark',
    colId: 'internalRemark',
    headerName: 'GridFailure.InternalRemark',
    sortable: true,
    comparator: stringInsensitiveComparator,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'postcode',
    colId: 'postcode',
    headerName: 'GridFailure.Postcode',
    sortable: true,
    comparator: stringInsensitiveComparator,
    suppressMovable: true,
    filter: 'setFilterComponent',
    valueGetter: params => (params.data.postcode ? params.data.postcode : params.data.freetextPostcode),
  },
  {
    field: 'city',
    colId: 'city',
    headerName: 'GridFailure.City',
    sortable: true,
    comparator: stringInsensitiveComparator,
    suppressMovable: true,
    filter: 'setFilterComponent',
    valueGetter: params => (params.data.city ? params.data.city : params.data.freetextCity),
  },
  {
    field: 'district',
    colId: 'district',
    headerName: 'GridFailure.District',
    sortable: true,
    comparator: stringInsensitiveComparator,
    suppressMovable: true,
    filter: 'setFilterComponent',
    valueGetter: params => (params.data.district ? params.data.district : params.data.freetextDistrict),
  },
  {
    field: 'street',
    colId: 'street',
    headerName: 'GridFailure.Street',
    sortable: true,
    comparator: stringInsensitiveComparator,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'housenumber',
    colId: 'housenumber',
    headerName: 'GridFailure.Housenumber',
    sortable: true,
    comparator: sortAlphaNum,
    suppressMovable: true,
    filter: 'setFilterComponent',
  },
  {
    field: 'tools',
    headerName: ' ',
    pinned: 'right',
    maxWidth: 110,
    minWidth: 110,
    lockPosition: true,
    sortable: false,
    filter: false,
    suppressMenu: true,
    suppressSizeToFit: true,
    headerComponent: 'headerCellRendererComponent',
    cellRenderer: IconCellRendererComponent,
  },
];
