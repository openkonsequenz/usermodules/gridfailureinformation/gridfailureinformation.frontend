/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { VisibilityConfigurationInterface } from '@openk-libs/grid-failure-information-map/shared/models/visibility-configuration.interface';
import { VisibilityEnum } from '@openk-libs/grid-failure-information-map/constants/enums';

// Convert datetime from ISO to local string
export function convertISOToLocalDateTime(value: string): string {
  if (value) {
    const date: Date = new Date(value);
    date.setHours(date.getHours() + date.getTimezoneOffset() / 60);
    return (
      padNumber(date.getDate()) +
      '.' +
      padNumber(date.getMonth() + 1) +
      '.' +
      date.getFullYear() +
      ' / ' +
      padNumber(date.getHours()) +
      ':' +
      padNumber(date.getMinutes())
    );
  } else {
    return '';
  }
}

export function padNumber(value: number) {
  if (isNumber(value)) {
    return `0${value}`.slice(-2);
  } else {
    return '';
  }
}

export function isNumber(value: any): value is number {
  return !isNaN(toInteger(value));
}

export function toInteger(value: any): number {
  return parseInt(`${value}`, 10);
}

export function determineDetailFieldVisibility(config: VisibilityConfigurationInterface, config_prop: string, field: string): boolean {
  if (config && config[config_prop]) {
    return config[config_prop][field] === VisibilityEnum.SHOW;
  } else {
    return true;
  }
}
